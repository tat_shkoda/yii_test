<?php

namespace app\models;

use \yii\db\ActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\User;

class Pet extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pets';
    }

    public function rules()
    {
        return [
            // ['status', 'validateStatus'],
            [['alias', 'birth_date', 'sex'], 'required'],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['alias', 'string', 'min' => 2, 'max' => 64],
            ['birth_date', 'validateDate'],
            ['sex', 'validateSexValue'],
            [['create_date', 'update_date'], 'safe']
        ];
    }

    public function validateSexValue()
    {
        if (! in_array($this->sex, [0, 1, 2])) {
            $this->addError('sex', 'sex имеет недопустимое значение');
        }
    }

    public function validateDate()
    {
        $currentDate = Yii::$app->getFormatter()->asDate(time());

        if ($currentDate < $this->birth_date){
            $this->addError('birth_date', 'birth_date не может быть больше текущей даты');
        }
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\BaseActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                    \yii\db\BaseActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],

                ],
                'value' => function(){
                    return gmdate("Y-m-d H:i:s");
                },
            ],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function extraFields()
    {
        return ['user'];
    }
}
