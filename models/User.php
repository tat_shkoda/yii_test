<?php

namespace app\models;

use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\models\Pet;
use yii\behaviors\OptimisticLockBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class User extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['name', 'last_name', 'email'], 'required'],
            [['name', 'last_name'], 'string', 'min' => 3, 'max' => 64],
            ['email', 'string', 'max' => 64],
            ['email', 'unique'],
            ['status', 'boolean', 'trueValue' => 1, 'falseValue' => 0, 'strict' => 1],
            [['create_date', 'update_date'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            OptimisticLockBehavior::className(),
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\BaseActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                    \yii\db\BaseActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],

                ],
                'value' => function(){
                    return gmdate("Y-m-d H:i:s");
                },
            ],

            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'isDeleted' => true
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }

    public function getPets()
    {
        return $this->hasMany(Pet::className(), ['user_id' => 'id']);
    }

    public function extraFields()
    {
        return ['pets'];
    }

    public function optimisticLock()
    {
        return 'version';
    }
}
