<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pets`.
 */
class m181223_084711_create_pets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pets', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'alias' => $this->string(64),
            'birth_date' => $this->string(64)->notNull(),
            'sex' => $this->tinyInteger(1),
            'status' => $this->boolean()->defaultValue(true),
            'create_date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'update_date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pets');
    }
}
