<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m181223_084825_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32),
            'last_name' => $this->string(32),
            'email' => $this->string(64),
            'status' => $this->boolean()->defaultValue(true),
            'create_date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'update_date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
