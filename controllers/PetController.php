<?php

namespace app\controllers;

use yii\rest\ActiveController;

class PetController extends ActiveController
{
    public $modelClass = 'app\models\Pet';
}
